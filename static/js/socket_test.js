var socket;
$(document).ready(function () {
    socket = io.connect("http://" + document.domain + ":" + location.port + '/chat');

    socket.on('connect', function () {
        console.log('socket connected');
    });

    socket.on('response', function (msg) {
        $("#received").append('<p> ' + msg.data + '</p>');
    });
    $("form#broadcast").submit(function (event) {
        if ($("#input-data").val() == "") {
            return false;
        }
        socket.emit("request", {data: $("#input-data").val()});
        $("#input-data").val("");
        return false;
    });
});

function send_text() {
    socket.emit('request', {data: $("#input-data").val()});
    return false;
}